import pygame
import requests
import urllib.request
import io

search_api_server = "https://static-maps.yandex.ru/1.x/"
ll = '50,50'
z = '2'
search_params = {
    "l": 'sat',
    "ll": ll,
    "lang": "ru_RU",
    "format": 'json',
    "z": z,
    "size": '650,450'
}
try:
    response = requests.get(search_api_server, params=search_params)
except:
    print("Something goes wrong")


page = urllib.request.urlopen(response.url)
image = page.read()
byte = io.BytesIO(image)
fon = pygame.image.load(byte)

screen = pygame.display.set_mode((650, 450))
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    screen.blit(fon, (0, 0))
    pygame.display.flip()

pygame.quit()
