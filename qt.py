import sys
import random
from urllib import request
import requests
from PyQt5 import uic
from PyQt5.QtCore import Qt as qt
from PyQt5.QtWidgets import QWidget, QLabel
from PyQt5.QtWidgets import QApplication, QHBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtGui import QColor, QPixmap
from PyQt5 import Qt
from PyQt5.QtWidgets import QInputDialog
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtGui import QPainter
import requests
import urllib.request
import io


class MyWidget(QMainWindow):

    def __init__(self):

        super().__init__()
        self.search_api_server = "https://static-maps.yandex.ru/1.x/"
        self.ll = '50,50'
        self.z = '2'
        self.params = {
                        "l": 'map',
                        "ll": self.ll,
                        "lang": "ru_RU",
                        "format": 'json',
                        "z": self.z,
                        "size": '650,450'
                      }
        try:
            self.response = requests.get(self.search_api_server, params=self.params)
        except:
            print("Something goes wrong")
        uic.loadUi('Maps.ui', self)
        self.setWindowTitle('Maps')
        self.update()

    def update(self):

        hbox = QHBoxLayout(self)
        data = request.urlopen(self.response.url).read()
        pixmap = Qt.QPixmap()
        pixmap.loadFromData(data)
        self.lbl.setPixmap(pixmap)
        hbox.addWidget(self.lbl)
        self.setLayout(hbox)
        self.show()
        #self.label.setText('*' * len(self.word))

    def keyPressEvent(self, e):

        cords = self.ll.split(',')
        if e.key() == qt.Key_PageUp and int(self.params['z']) < 17:
            self.params['z'] = str(int(self.params['z']) + 1)
        elif e.key() == qt.Key_PageDown and int(self.params['z']) > 1:
            self.params['z'] = str(int(self.params['z']) - 1)
        elif e.key() == qt.Key_Up and int(self.params['ll']) > 1:
            self.params['ll'] = cords[0] + ',' + str(int(cords[1]) + 360 / 2**z)
        elif e.key() == qt.Key_Down and int(self.params['ll']) > 1:
            self.params['ll'] = str(int(self.params['ll']) - 1)
        elif e.key() == qt.Key_Left and int(self.params['ll']) > 1:
            self.params['ll'] = str(int(self.params['ll']) - 1)
        elif e.key() == qt.Key_Right and int(self.params['ll']) > 1:
            self.params['ll'] = str(int(self.params['ll']) - 1)
        try:
            self.response = requests.get(self.search_api_server, params=self.params)
        except:
            print("Something goes wrong")
        self.update()

app = QApplication(sys.argv)
ex = MyWidget()
ex.show()
sys.exit(app.exec_())
