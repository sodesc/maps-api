import pygame
import requests
import urllib.request
import io

search_api_server = "https://static-maps.yandex.ru/1.x/"
ll = ['50', '50']
z = '2'
search_params = {
    "l": 'map',
    "ll": ','.join(ll),
    "lang": "ru_RU",
    "format": 'json',
    "z": z,
    "size": '650,450'
}

screen = pygame.display.set_mode((650, 450))
running = True

try:
    response = requests.get(search_api_server, params=search_params)
except:
    print("Something goes wrong")

def get_map(res):
    response = res
    page = urllib.request.urlopen(response.url)
    image = page.read()
    byte = io.BytesIO(image)
    return pygame.image.load(byte)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            cords = search_params['ll'].split(',')
            if event.key == pygame.K_PAGEUP and int(search_params['z']) < 17:
                search_params['z'] = str(int(search_params['z']) + 1)
            elif event.key == pygame.K_PAGEDOWN and int(search_params['z']) > 1:
                search_params['z'] = str(int(search_params['z']) - 1)
            elif event.key == pygame.K_UP:
                search_params['ll'] = cords[0] + ',' + str(int(cords[1]) + 1)
            elif event.key == pygame.K_DOWN:
                pass#search_params['ll']
            elif event.key == pygame.K_RIGHT:
                pass#search_params['ll']
            elif event.key == pygame.K_LEFT:
                pass#search_params['ll']
            try:
                response = requests.get(search_api_server, params=search_params)
            except:
                print("Something goes wrong")
    screen.blit(get_map(response), (0, 0))
    pygame.display.flip()

pygame.quit()
